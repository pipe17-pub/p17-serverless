#!/usr/bin/env node
const resolve = require('path').resolve
const merge = require('@alexlafroscia/yaml-merge')
const fs = require('fs')
const shell = require('shelljs')

const setIgnore = () => {
    let filesToIgnore = {
        'serverless-base.yml': true,
        'serverless.yml': true,
        '.serverless': true,
    }
    if (fs.existsSync(resolve('.gitignore'))) {
        const fileStr = fs.readFileSync(resolve('.gitignore'), { encoding: 'utf8', flag: 'r' })
        const lines = fileStr.split('\n')
        for (let line of lines) {
            if (line.startsWith('./')) {
                line = line.substring(2)
            }
            if (line.endsWith('/')) {
                line = line.substring(0, line.length - 1)
            }
            if (filesToIgnore[line]) {
                console.log(`${line} already in .gitignore`)
                filesToIgnore[line] = false
            }
        }
    }
    for (const file of Object.keys(filesToIgnore)) {
        if (filesToIgnore[file]) {
            console.log(`Adding ${file} to .gitignore`)
            const cmd = `echo '\n${file}' >> .gitignore`
            shell.exec(cmd)
        }
    }
}

const setup = (overwriteFilename) => {
    setIgnore()
    shell.cp(`${__dirname}/serverless-base.yml`, '.')
    console.log(`Merging ${overwriteFilename} onto serverless-base.yml to generate serverless.yml`)
    const output = merge(resolve('serverless-base.yml'), resolve(overwriteFilename))
    fs.writeFileSync(resolve('serverless.yml'), output)
}

const main = () => {
    if (fs.existsSync(resolve('serverless-overwrite.yml'))) {
        let overwriteFilename = 'serverless-overwrite.yml'

        for (let i = 0; i < process.argv.length; i++) {
            const arg = process.argv[i]
            if (arg === '-c' || arg === '--config') {
                if (i + 1 < process.argv.length) {
                    overwriteFilename = process.argv[i + 1]
                    process.argv.splice(i, 2)                
                }
                break
            }
        }
    
        if (fs.existsSync(resolve(overwriteFilename))) {
            setup(overwriteFilename)
        }
    }

    process.argv.shift()
    process.argv[0] = 'serverless'
    let hasStage
    for (const arg of process.argv) {
        if (arg === '-s' || arg === '--stage') {
            hasStage = true
            break
        }
    }
    if (!hasStage && process.env.CI_COMMIT_BRANCH) {
        process.argv.push('--stage')
        process.argv.push(process.env.CI_COMMIT_BRANCH)
    }
    const cmd = process.argv.join(' ')
    console.log(cmd)
    const result = shell.exec(cmd)
    const code = result.code
    process.exit(code)
}
main()
