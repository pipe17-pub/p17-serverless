# serverless-offline-ssm
The official serverless-offline-ssm plugin is not working with `variablesResolutionMode: 20210326`

This is a fix from this Github issue https://github.com/janders223/serverless-offline-ssm/issues/145#issuecomment-850429523

This is temporary anyway, when we upgrade to Serverless v3 it will change again
