class OfflineSSM {
  constructor(serverless, options) {
    this.serverless = serverless;
    this.service = serverless.service;
    this.config = (this.service.custom && this.service.custom['serverless-offline-ssm']) || {};
    this.stage = serverless.service.provider.stage;

    if (!this.shouldExecute()) {
      return;
    }

    const aws = this.serverless.getProvider('aws');
    const originalRequest = aws.request.bind(aws);

    aws.request = (service, method, params) => {
      if (service !== 'SSM' || method !== 'getParameter') {
        return originalRequest(service, method, params, options);
      }
      let { Name } = params;
      if (Name.startsWith('/')) {
        // Remove leading /
        Name = Name.substring(1)
      }
      if (Name.startsWith('aws/reference/secretsmanager/')) {
        Name = Name.substring('aws/reference/secretsmanager/'.length)
      }
      let value = process.env[Name] || this.config.ssm[Name];
      let returnType = 'String'
      if (value.startsWith('{')) {
        returnType = 'SecureString'
      }
      const Parameter = {
        Type: returnType,
        Value: value,
      }
      return Promise.resolve({
        Parameter,
      });
    };

    this.serverless.setProvider('aws', aws);
  }

  shouldExecute() {
    if (this.config.stages && this.config.stages.includes(this.stage)) {
      return true;
    }
    return false;
  }
}

module.exports = OfflineSSM;